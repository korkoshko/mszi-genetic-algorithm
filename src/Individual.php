<?php

namespace korkoshko;

class Individual
{
    /**
     *
     */
    public const MAX_BITS = 16;

    /**
     * @var int|null
     */
    protected ?int $number;

    /**
     * @var string|null
     */
    protected ?string $binary;

    /**
     * Individual constructor.
     *
     * @param int|string|null $number
     */
    public function __construct($number = null)
    {
        $number = is_string($number) ? (int) bindec($number) : $number;

        $this->number = $number ?? $this->generate();
        $this->binary = $this->toBinary($this->number);
    }

    /**
     * @return int|null
     */
    public function getNumber(): ?int
    {
        return $this->number;
    }

    /**
     * @return string|null
     */
    public function getBinary(): ?string
    {
        return $this->binary;
    }

    /**
     * @param int $percent
     *
     * @return Individual
     * @throws \Exception
     */
    public function mutate(int $percent): self
    {
        if ($this->isAllowMutate($percent)) {
            $genForMutate = random_int(1, self::MAX_BITS - 1);
            $this->binary[$genForMutate] = (int) $this->binary[$genForMutate] ? '0' : '1';
            $this->number = $this->toDecimal($this->binary);
        }

        return $this;
    }

    /**
     * @param int $number
     *
     * @return string
     */
    protected function toBinary(int $number): string
    {
        return decbin($number);
    }

    /**
     * @param string $binary
     *
     * @return int
     */
    protected function toDecimal(string $binary): int
    {
        return bindec($binary);
    }

    /**
     * @return int
     * @throws \Exception
     */
    protected function generate(): int
    {
        return rand(0, 0x0fff) | 0x8000;
    }

    /**
     * @param int $percent
     *
     * @return bool
     */
    private function isAllowMutate(int $percent): bool
    {
        return $percent >= random_int(0, 100);
    }
}